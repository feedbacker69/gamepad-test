let currentlySelectedGamepad = null;
let beginTime = null;
let currentSetIntervalId = null;

function refreshGamepadList() {
  const select = document.querySelector("#gamepadList");
  if (!select) return;

  const gamepads = navigator.getGamepads();

  const newGamepadOptions = gamepads
    .filter((gamepad) => gamepad)
    .map((gamepad) => {
      const option = document.createElement("option");
      const label = document.createTextNode(gamepad?.id || "unavailable");
      if (gamepad) option.id = gamepad.id;
      option.appendChild(label);
      return option;
    });

  select.replaceChildren(...newGamepadOptions);
}

function releaseTimer() {
  window.clearTimeout(currentSetIntervalId);
  currentSetIntervalId = null;
  currentlySelectedGamepad = null;
  beginTime = null;

  const button = document.querySelector("#timerButton");
  button.disabled = false;
}

function startTimer() {
  const select = document.querySelector("#gamepadList");
  const button = document.querySelector("#timerButton");

  button.disabled = true;

  const gamepads = navigator.getGamepads();

  const gamepadToBeSelected = gamepads.find(
    (gamepad) => gamepad?.id === select.options[select.selectedIndex].id
  );

  if (gamepadToBeSelected) {
    currentlySelectedGamepad = gamepadToBeSelected;
    beginTime = Date.now();
    currentSetIntervalId = window.setInterval(update, 1000);
  }
}

function formatDate(date) {
  const hh = new String(date.getUTCHours()).padStart(2, "0");
  const mm = new String(date.getUTCMinutes()).padStart(2, "0");
  const ss = new String(date.getUTCSeconds()).padStart(2, "0");

  return `${hh}:${mm}:${ss}`;
}

function update() {
  if (!currentlySelectedGamepad || beginTime === null) return;
  const results = document.querySelector("#results");

  const gamepads = navigator.getGamepads();

  const gamepadIsAlive = gamepads.find(
    (gamepad) => gamepad?.id === currentlySelectedGamepad.id
  );

  if (!gamepadIsAlive) {
    results.style.color = "red";
    releaseTimer();
    return;
  }

  const timeNow = Date.now();

  const dateInterval = new Date(timeNow - beginTime);

  const text = `Been alive for: ${formatDate(dateInterval)}`;

  const paragraph = document.createElement("p");
  paragraph.textContent = text;

  results.replaceChildren(paragraph);
}

window.addEventListener("gamepadconnected", refreshGamepadList);

window.addEventListener("gamepaddisconnected", (e) => {
  if (e.gamepad.id === currentlySelectedGamepad.id) {
    update();
  }
});
